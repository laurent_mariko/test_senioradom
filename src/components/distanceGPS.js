
import React from 'react';
import axios from 'axios';

export default class DistanceGPS extends React.Component{

    username = "name"
    password = "namepwd"
    constructor({position1, position2}){
      super()
      this.position1 = position1;
      this.position2 = position2;
    }
  
    configuration = {
      headers: { 
        'Access-Control-Allow-Origin': 'origin-list'
        }
      }
    
    state = {
      data : 0.0
    }
    
    async componentDidMount() {
      await axios.get(`http://localhost:9000/distance/${this.position1.id}/${this.position2.id}`,this.configuration)
        .then(res => {
          const distance = res.data;
          this.setState({data: distance}) ;
        })
    }
  
    render() {
      return (
        <div>
            <h3>La distance entre les positions choisies est : {this.state.data} m</h3>
        </div>
      )
    }
  }