import React from 'react'
import axios from 'axios'

export default class AddPositionForm extends React.Component {
    constructor({statePosition, updatestatePosition}) {
      super();
      this.state = {};
      this.statePosition = statePosition
      this.updatestatePosition = updatestatePosition
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
    
    handleSubmit(event) {
        var regex=/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/;
        
        
        if(typeof this.state.latitude === 'undefined' || this.state.latitude === "" || !this.state.latitude.match(regex) || this.state.latitude < 0) {
            alert('Les valeurs négatives ou non numériques sont interdites.')
            return
        }
        if(typeof this.state.longitude === 'undefined' || this.state.longitude === "" || !this.state.longitude.match(regex)|| this.state.longitude < 0) {
            alert('Les valeurs négatives ou non numériques sont interdites.')
            return
        }
        try{
          
          var request = new XMLHttpRequest();
          request.open('GET', 'http://localhost:9000/positions/', false);  // `false` makes the request synchronous
          request.send(null);

          if (request.status !== 200) {
            console.log(request.responseText);
            alert("Une erreur s'est produite.\n")
            return
          }

          axios.post('http://localhost:9000/position', this.state)
            .then(response =>{ 
                this.setState({data:{ id: response.data.id,  latitude: response.data.latitude,  longitude: response.data.longitude  }})
                }
                )
                
            .catch(error => {
                this.setState({ errorMessage: error.message });
                throw 'An error has occurred:' + error.message
            });
          }catch(error){
            console.error('An error has occurred', error.message);
            alert("Une erreur s'est produite.\n" + error.message)
            
            alert("Une erreur s'est produite.\n")
            return
          }
          alert("La position a été ajoutée.\n")
          window.location.reload();
    }

    value = {id:"", latitude: "", longitude: ""}

	handleForm = (event) =>{
        
		this.setState({[event.target.name] : event.target.value});
	}

    render() {
      return (
        <form>
        <label>Latitude</label><br/>
        <input type="text" name="latitude" onChange={this.handleForm} /><br/>

        <label>Longitude</label><br/>
        <input type="text" name="longitude" onChange={this.handleForm} /><br/>
        <button type="button" onClick={this.handleSubmit}>Ajouter</button>
      </form>
      );
    }
  }