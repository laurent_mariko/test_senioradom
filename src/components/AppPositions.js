import React from 'react'
import CartPositions from './CartPositions'
import {useState} from 'react'
import PositionList from './PositionList'
import AddPositionForm from './AddPositionForm';
function App() {
  const [cart, updateCart] = useState([])
  const [statePosition, updatestatePosition] = useState({data : []})

  return (
  
  <div>
    <CartPositions cartPositions={cart} updateCart={updateCart} />
    <AddPositionForm  updatestatePosition={updatestatePosition}/>
    <PositionList  cartPositions={cart} updateCart={updateCart} statePosition={statePosition} updatestatePosition={updatestatePosition} />
    <br />
    <br />
  </div>
  );
}

export default App;
