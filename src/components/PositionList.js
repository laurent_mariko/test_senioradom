import React from 'react'
import Positions from "./positions"
function PositionList({ cartPositions, updateCart, statePosition, updatestatePosition}) {
	return (
    <div>
        <ul>
            <Positions 
                updateCart = {updateCart} 
                cartPositions={cartPositions}
                state={statePosition}
                updatestatePosition={updatestatePosition}
			/>
				
        </ul>
    </div>
    
    )
}
export default PositionList