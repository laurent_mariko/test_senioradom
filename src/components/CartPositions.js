import React from 'react'
import '../styles/Cart.css'
import PositionItem from "./PositionItem"
import DistanceGPS from './distanceGPS';

function emptyList(cartPositions, updateCart) {
    while (cartPositions.length > 0) {
        cartPositions.pop();
    }
    updateCart([])
}

function calculerDistance(cartPositions) {
    return cartPositions.length >= 2 ?
        (<DistanceGPS position1={cartPositions[0]} position2 = {cartPositions[1]} />):<br />
}

function CartPositions({ cartPositions, updateCart }){
    return cartPositions.length> 0 ?(
        
        <div className='lmj-cart'>
            <h1><button onClick={() => emptyList(cartPositions, updateCart)}> Vider la liste des positions </button></h1>
            
            <h2>Liste des positions</h2>
            <ul>
            {cartPositions.map((position) => (
                
                <div key={position.id}>
					<PositionItem
						id={position.id}
						latitude= {position.latitude}
                        longitude= {position.longitude}
					/>
                    <br />
                </div>
				))}
            </ul>
            <h3>{calculerDistance(cartPositions)}</h3>
         </div>
        
        
    ): <br/>

}
export default CartPositions