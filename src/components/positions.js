/* eslint-disable no-sequences */
import React from 'react'
import axios from 'axios'
import Position from './Position'


export default class Positions extends React.Component{

  username = "name"
  password = "namepwd"
  message = "Seulement deux positions peuvent être ajoutées."
  constructor({updateCart, cartPositions,state,updatestatePosition}){
    super()
    this.updateCart = updateCart;
    this.cartPositions = cartPositions;
    this.statePos = state
    this.updatestatePosition = updatestatePosition
  }

  state = {data : []}

  configuration = {
    headers: { 
      'Access-Control-Allow-Origin': 'origin-list'
      }
    }
  
  async componentDidMount() {
    await axios.get(`http://localhost:9000/positions`, this.configuration)
      .then(res => {
        const positions = res.data;
        this.setState({data: positions})
        this.statePos.data = {data: positions}
        this.updatestatePosition({data: positions})
      })
  }

  addPosition(id, latitude, longitude, message) {
    const retrievedObject = this.cartPositions.filter(position => position.id === id) 
    if(retrievedObject.length === 0){
        this.cartPositions = [...this.cartPositions, new Position(id, latitude, longitude)]
        this.cartPositions.length <= 2? this.updateCart(this.cartPositions):alert(message)
        
    }else {
      alert("Cette position est déjà présente dans la liste.")
    }
  }

  removePosition(cartPositions, id){
    if(window.confirm("Etes vous sûr?")){
        axios.delete(`http://localhost:9000/position/${id}`,this.configuration)
                .then(response => {alert('Suppression effectuée');this.render()})
                .catch(error => {
                    alert(error.message);
                    console.error('An error has occurred!', error);
        }
        );
        
        let positionsCartState = []
        for( let i = 0; i < this.cartPositions.length; i++){ 
          if ( this.cartPositions[i].id !== id) { 
            positionsCartState.push(this.cartPositions[i]); 
          }
        }
        this.cartPositions = positionsCartState
        this.updateCart(positionsCartState)
        
        let positions = []
        for( let i = 0; i < this.state.data.length; i++){ 
          if (this.state.data[i].id !== id) { 
            positions.push(this.state.data[i])
          }
        }
        this.setState( { data: positions}) ;
        
    }
  }

  render() {
    
    return (this.state.data.sort((positionX, positionY) => positionY.id - positionX.id).map(({id, latitude, longitude}) => (
      <div key={id}>
        <li>
            id : {id} <br />
            latitude : {latitude} <br />
            longitude : {longitude} <br />
        </li>
        <br />
        <button onClick={() => this.addPosition(id, latitude, longitude, this.message)}>
            Ajouter 
        </button>
        <button onClick={() =>this.removePosition (this.cartPositions, id)}>
            Supprimer 
        </button>
      </div>
    )))
    
  }
}
