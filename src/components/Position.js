export default class Position {
    constructor(id, latitude, longitude) {
        this.id = id
        this.latitude = latitude;
        this.longitude = longitude;
    };
    toString() {
        return (`Id : ${this.id}, Latitude : ${this.latitude}, Longitude : ${this.longitude}`);
    }
}    