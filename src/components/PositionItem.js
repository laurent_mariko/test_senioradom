import React from 'react'
function PositionItem({id, latitude, longitude}) {
    return(
        <li key={id}>
            id : {id} <br />
            latitude : {latitude} <br />
            longitude : {longitude} <br />
        </li>
    )
}

export default PositionItem