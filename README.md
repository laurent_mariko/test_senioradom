Procedure d'utilisation

Procédure sans docker:

1) Lancement du back: 
  a) Installer Java 11
  b) Télécharger les sources à partir de git dans un répertoire:
     git clone https://laurent_mariko@bitbucket.org/laurent_mariko/test_senioradom_back.git
  c) Se placer dans le répertoire créé test_senioradom_back
  d) Lancer la commande 
	 mvnw.cmd clean install
  e) Aller dans le répertoire target
  f) Lancer la commande suivante:
     java -jar TestSeniorADom-0.0.1-SNAPSHOT.jar
	 
2) Lancement du front:
 a) Télécharger les sources à partir de git dans un répertoire
	git clone https://bitbucket.org/laurent_mariko/test_senioradom.git
	
 b) Télécharger et installer nodejs, le cas échéant
	
 c) Dans le répertoire parent des sources téléchargées:
	npx create-react-app test_senioradom2
 
 d) Recopier dans le répertoire test_senioradom2 les sources téléchargées à partir de git du répertoire test_senioradom.
 
 e) Installer naxios
    npm install nagios-plugin

3) Créer une base de données: "api" dans Posgresql

4) Configurer la base de données en fonction des paramètres suivants:
spring.datasource.url=jdbc:postgresql://localhost:5432/api
spring.datasource.username=postgres
spring.datasource.password="a définir"
spring.sql.init.platform=postgresql

Procédure avec Docker:

1) Installer Docker et docker-compose le cas échéant

2) Dans un répertoire lancer la commande suivante:
   git clone https://laurent_mariko@bitbucket.org/laurent_mariko/docker_senioradom.git

3) Se placer dans le répertoire docker_senioradom créé

4) Lancer la commande:
   docker-compose up